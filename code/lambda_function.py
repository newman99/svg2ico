"""Create favicions and other icons from an SVG file."""
import json
import subprocess
import os
import uuid
from zipfile import ZipFile
import boto3


origin_bucket = 'svg2ico'
destination_bucket = 'svg2ico-output'
filename = 'card.svg'
resource = boto3.resource('s3')


def lambda_handler(event, context):
    """Lambda handler."""
    try:
        filename = event['Records'][0]['s3']['object']['key']
    except KeyError:
        return {
            'statusCode': 200,
            'body': json.dumps({"Error": "File not found"})
        }
    resource.Object(
        origin_bucket, filename).download_file('/tmp/{}'.format(filename))
    subprocess.run(['/bin/bash', 'convert.sh', filename])
    images = os.listdir('/tmp')
    zipfile = "favicons-{}.zip".format(str(uuid.uuid4().hex))
    with ZipFile('/tmp/{}'.format(zipfile), 'w') as zip:
        for file in images:
            zip.write('/tmp/{}'.format(file), arcname=file)
    resource.Object(
        destination_bucket, zipfile).upload_file('/tmp/{}'.format(zipfile))
    return {
        'statusCode': 200,
        'body': json.dumps({'zipfile': zipfile})
    }
