#!/bin/sh

# run using ./svg2ico.sh FILENAME.svg

LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/lib:/lib/lib64"
PATH=$PATH

FILE=$1

EXT=`echo $1 | awk -F . '{print $NF}'`
BASE=`basename $FILE .$EXT`

if [ -f /tmp/$FILE ];
then
    case "$EXT" in
        svg )
            echo Good, a svg file, creating favicon.ico
        ;;
        *)
            echo Must use an svg image
            exit
        ;;
    esac
else
    echo no file $FILE
    exit
fi

/opt/bin/convert -background transparent MSVG:/tmp/$FILE -define icon:auto-resize=64,48,32,16 /tmp/favicon.ico

for SIZE in 57 60 72 76 114 120 144 152
do
    /opt/bin/convert -background transparent -resize x$SIZE /tmp/$FILE /tmp/apple-touch-icon-${SIZE}x${SIZE}.png
done

for SIZE in 16 32 96 128
do
    /opt/bin/convert -background transparent -resize x$SIZE /tmp/$FILE /tmp/favicon-${SIZE}.png
done

for SIZE in 128 270 558
do
    /opt/bin/convert -background transparent -resize x$SIZE /tmp/$FILE /tmp/mstile-${SIZE}x${SIZE}.png
done

cat << EOF > /tmp/favicon.html
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="favicon-128.png" sizes="128x128" />
<meta name="application-name" content="&nbsp;"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-square70x70logo" content="mstile-128x128.png" />
<meta name="msapplication-square150x150logo" content="mstile-270x270.png" />
<meta name="msapplication-square310x310logo" content="mstile-558x558.png" />
EOF
