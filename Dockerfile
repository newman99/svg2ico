FROM lambci/lambda:python3.7

ENV PYTHONUNBUFFERED 1

MAINTAINER "Matthew Newman" <newman99@gmail.com>

WORKDIR /var/task

CMD ["bash"]
